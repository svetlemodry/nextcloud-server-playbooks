---

- name: "Nextcloud server upgrade playbook"
  hosts: zion
  become: yes

  vars_prompt:
    - name: sql_db_password
      prompt: Enter SQL DB password
      private: yes
    - name: server_fqdn
      prompt: Enter web server FQDN
      private: no
    - name: upgrade_to_version
      prompt: Enter Nextcloud version to upgrade to (e.g. 24.0.6)
      private: no

  vars:
    server_nextcloud_path: "/srv/http/{{ server_fqdn }}/nextcloud"
    upgrade_dir_with_timestamp: "nc/nc-upg-{{ ansible_date_time.iso8601_basic_short }}"
    web_server_uid: "http"


  tasks:

  - name: "Get current Nextcloud version"
    ansible.builtin.shell:
      cmd: sudo -u {{ web_server_uid }} php ./occ --version | sed 's/ //g'
      chdir: "{{ server_nextcloud_path }}"
    register: current_nc_version

  - name: "Check if upgrade version same or lower than current, if yes stop upgrade"
    ansible.builtin.fail:
      msg: "Running Nextcloud version same or lower than the one to upgrade"
    when: current_nc_version.stdout is version( upgrade_to_version, '>=', version_type='semver')

# # Explicit listing for debug purpose
#   - name: "Ansible distribution and hostname fact debug info"
#     debug:
#       msg:
#       - "distro: {{ ansible_distribution }}"
#       - "hostname: {{ ansible_hostname }}"

  - name: "Check if server is Arch Linux"
    ansible.builtin.fail:
      msg: "Unsupported platform! This playbook is for Arch Linux!"
    when: ansible_distribution not in ["Archlinux"]

  - name: "Check if server hostname is ZION"
    ansible.builtin.fail:
      msg: "Wrong server! This playbook is for ZION!"
    when: ansible_hostname not in ["zion"]

  - name: "Download Nextcloud server tarball v{{ upgrade_to_version }}"
    ansible.builtin.get_url:
      url: "https://download.nextcloud.com/server/releases/nextcloud-{{ upgrade_to_version }}.tar.bz2"
      dest: "/root/nextcloud-{{ upgrade_to_version }}.tar.bz2"
      mode: '0644'

  - name: "Create a temporary upgrade directory under /home/{{ upgrade_dir_with_timestamp }} if it does not exist"
    ansible.builtin.file:
      path: /home/{{ upgrade_dir_with_timestamp }}
      state: directory
      mode: '0755'

  - name: "Retrieve list of currently installed nextcloud apps"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ app:list
      chdir: "{{ server_nextcloud_path }}"
    register: nc_apps_list

  - name: "Save a list of currently installed nextcloud apps"
    ansible.builtin.copy:
      content: "{{ nc_apps_list.stdout }}"
      dest: /home/{{ upgrade_dir_with_timestamp }}/installed_nextcloud_apps.txt

# # List installed Nexcloud apps - for debuging
#   - name: "List installed Nexcloud apps"
#     ansible.builtin.debug:
#       msg: "{{ nc_apps_list }}"

  - name: "Empty trashbins for all Nextcloud users before running server data backup"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ trashbin:cleanup --all-users
      chdir: "{{ server_nextcloud_path }}"

  - name: "Enable Nextcloud maintenance mode"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ maintenance:mode --on
      chdir: "{{ server_nextcloud_path }}"

  - name: "Backup current nextcloud installation - files"
    community.general.archive:
      path: "{{ server_nextcloud_path }}"
      dest: "/home/{{ upgrade_dir_with_timestamp }}/nextcloud-files-{{ ansible_date_time.iso8601_basic_short }}.tar"
      format: tar

  - name: "Backup current Nextcloud installation - database"
    ansible.builtin.shell:
      cmd: "mysqldump -u nextcloud --password='{{ sql_db_password }}' nextcloud | gzip > /home/{{ upgrade_dir_with_timestamp }}/nextcloud-sql-bckup-{{ ansible_date_time.iso8601_basic_short }}.sql.gz"
      chdir: "{{ server_nextcloud_path }}"
    no_log: yes

  - name: "Create the unpack directory under /root/"
    ansible.builtin.file:
      path: "/root/nextcloud-{{ upgrade_to_version }}/"
      state: directory

  - name: "Unpack Nextcloud server version {{ upgrade_to_version }}"
    ansible.builtin.unarchive:
      src: "/root/nextcloud-{{ upgrade_to_version }}.tar.bz2"
      dest: "/root/nextcloud-{{ upgrade_to_version }}/"
      creates: "/root/nextcloud-{{ upgrade_to_version }}/nextcloud"
      remote_src: yes

  - name: "Change owner to {{ web_server_uid }} and set permissions in the extracted directory"
    ansible.builtin.file:
      path: "/root/nextcloud-{{ upgrade_to_version }}/"
      state: directory
      owner: "{{ web_server_uid }}"
      group: "{{ web_server_uid }}"
      mode: u=rwX,g=rX,o=rX
      recurse: yes

  - name: "Register unneeded directories under {{ server_nextcloud_path }}, exclude config/ and data/ dirs"
    ansible.builtin.find:
      paths: "{{ server_nextcloud_path }}"
      recurse: no
      hidden: yes
      file_type: any
      excludes: 'config,data'
    register: files_to_delete

  - name: "Remove old install Nextcloud server dirs and files, leave config/ and data/ dirs"
    ansible.builtin.file:
      path: "{{ item.path }}"
      state: absent
    with_items: "{{ files_to_delete.files }}"

  - name: "Copy over the new Nextcloud server files"
    ansible.builtin.copy:
      src: "/root/nextcloud-{{ upgrade_to_version }}/nextcloud/"
      dest: "{{ server_nextcloud_path }}"
      remote_src: yes
      force: yes
      owner: "{{ web_server_uid }}"
      group: "{{ web_server_uid }}"
      mode: u=rwX,g=rX,o=rX

  - name: "Upgrade the Nextcloud install"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ upgrade
      chdir: "{{ server_nextcloud_path }}"

  - name: "Disable maintenance mode for current installation"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ maintenance:mode --off
      chdir: "{{ server_nextcloud_path }}"

  - name: "Run add missing primary keys"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ db:add-missing-primary-keys -n
      chdir: "{{ server_nextcloud_path }}"

  - name: "Run add missing columns"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ db:add-missing-columns -n
      chdir: "{{ server_nextcloud_path }}"

  - name: "Run add missing indices"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ db:add-missing-indices -n
      chdir: "{{ server_nextcloud_path }}"

  - name: "Run convert filecache bigint"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ db:convert-filecache-bigint -n
      chdir: "{{ server_nextcloud_path }}"

  - name: "Confirm all Nextcloud apps are on latest version"
    ansible.builtin.command:
      cmd: sudo -u {{ web_server_uid }} php ./occ app:update --all -n
      chdir: "{{ server_nextcloud_path }}"

  - name: "Delete the unpack directory in /root if it still exists"
    ansible.builtin.file:
      path: "/root/nextcloud-{{ upgrade_to_version }}/"
      state: absent

  # - name: Delete the upgrade directory in /home if it still exists
  #   ansible.builtin.file:
  #     path: /home/{{ upgrade_dir_with_timestamp }}
  #     state: absent

